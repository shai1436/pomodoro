import React, { Component } from "react";
import { connect } from "react-redux";
import Clock from "./components/Clock";
import Header from "./components/Header";
import SessionList from "./components/SessionList";
import LineChart from "./components/LineChart";
import Footer from "./components/Footer";
import "./stylesheets/App.css";
import {
  addSession,
  changeSettings,
  removeSession,
  fetchSessions,
  fetchDayData,
  fetchSettings
} from "./ActionCreators";

const mapStateToProps = state => {
  return {
    pomodoroSessions: state.pomodoroSessions,
    pomodoroSettings: state.pomodoroSettings,
    pomodoroDayData: state.pomodoroDayData
  };
};

const mapDispatchToProps = dispatch => ({
  //addComment: (dishId, rating, author, comment) => dispatch(addComment(dishId, rating, author, comment)),
  addSession: (label, date, iteration, time) => {
    dispatch(addSession(label, date, iteration, time));
    //dispatch(addDayData(date, iteration, time));
  },

  fetchSessions: () => {
    dispatch(fetchSessions());
  },
  fetchDayData: () => {
    dispatch(fetchDayData());
  },
  fetchSettings: () => {
    dispatch(fetchSettings());
  },

  changeSettings: (
    playDuration,
    workDuration,
    playSound,
    workSound,
    breakAlert
  ) => {
    dispatch(
      changeSettings(
        playDuration,
        workDuration,
        playSound,
        workSound,
        breakAlert
      )
    );
  },
  removeSession: (id, date, time, iteration) => {
    dispatch(removeSession(id, date, time, iteration));
    //dispatch(removeDayData(date, time, iteration));
  }
});

class App extends Component {
  componentWillMount() {
    if (window.localStorage["jwt"]) {
      this.props.fetchSessions();
      this.props.fetchDayData();
      this.props.fetchSettings();
    } else {
      window.location.href = "/";
    }
  }

  render() {
    return (
      <div className="App">
        <Header
          settings={this.props.pomodoroSettings}
          changeSettings={this.props.changeSettings}
        />
        <div className="container mt-5 pt-3">
          <Clock
            work={this.props.pomodoroSettings.workDuration}
            play={this.props.pomodoroSettings.playDuration}
            addSession={this.props.addSession}
            settings={this.props.pomodoroSettings}
          />

          <SessionList
            sessions={this.props.pomodoroSessions}
            remove={this.props.removeSession}
            addSession={this.props.addSession}
          />
          <LineChart sessions={this.props.pomodoroDayData} />
          <Footer />
          <audio id="startSound">
            <source
              src="../assets/sounds/Winding_Alarm_Clock.mp3"
              type="audio/mpeg"
            />
          </audio>
          <audio id="breakSound">
            <source src="../assets/sounds/Ship_Bell.mp3" type="audio/mpeg" />
          </audio>
          <audio id="stopSound">
            <source
              src="https://s3.amazonaws.com/freecodecamp/simonSound4.mp3"
              type="audio/mpeg"
            />
          </audio>
        </div>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
