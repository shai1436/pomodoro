import C from './ActionTypes';
import axios from 'axios';
import {baseUrl} from './baseUrl';

axios.defaults.headers.common['Authorization'] = window.localStorage["jwt"];

const handleError = (error) => {
  localStorage.clear();
  window.location.href = "/";
}

export const addStateSession = (pomodoroSessions) => ({
  type: C.ADD_SESSION,
  pomodoroSessions: pomodoroSessions
});

export const addSession = (label, date, iteration, time) => (dispath) => {
  document.getElementById('root').classList.toggle("loading-mask");
  axios.post(baseUrl+ 'sessions', {
    label: label,
    date: date,
    iteration: parseInt(iteration, 10),
    time: parseInt(time, 10)
  },
    {
      headers: {
      "Authorization": window.localStorage["jwt"]
      }
    }
  )
  .then(function (response) {
    console.log("response", response.data);
    document.getElementById('root').classList.toggle("loading-mask");
    dispath(addStateSession(response.data.pomodoroSessions));
    dispath(addDayData( response.data.pomodoroDayData));
  })
  .catch(function (error) {
    console.log(error);
    document.getElementById('root').classList.toggle("loading-mask");
    handleError(error);
  });
};

export const addAllSessions = (sessions) => ({
  type: C.ADD_ALL_SESSIONS,
  sessions: sessions
});

export const fetchSessions = () => (dispatch) => {
  return axios.get(baseUrl+ 'sessions',
    {
      headers: {
      "Authorization": window.localStorage["jwt"]
      }
    }
  )
  .then(function (response) {
    // handle success
    return dispatch(addAllSessions(response.data));
  })
  .catch(function (error) {
    console.log("Error", error);
    handleError(error);
  })
  .then(function () {
    // always executed
  });
}
export const addDayData = (pomodoroDayData) => ({
  type: C.ADD_DAY_DATA,
  pomodoroDayData: pomodoroDayData
});

export const addAllDayData = (data) => ({
  type: C.ADD_ALL_DAY_DATA,
  dayData: data
});

export const fetchDayData = () => (dispatch) => {

  return axios.get(baseUrl+ 'dayData',
    {
      headers: {
      "Authorization": window.localStorage["jwt"]
      }
    }
  )
  .then(function (response) {
    // handle success
    return dispatch(addAllDayData(response.data));
  })
  .catch(function (error) {
    console.log(error);
    handleError(error);
  })
  .then(function () {
    // always executed
  });
}

// export const changeSettings = (playDuration, workDuration, playSound, workSound) => ({
//   type: C.CHANGE_SETTINGS,
//   playDuration: playDuration,
//   workDuration: workDuration,
//   playPlaySound: playSound,
//   playWorkSound: workSound
// });

export const changeSettings = (playDuration, workDuration, playSound, workSound, breakAlert) => (dispath) => {
  axios.post(baseUrl+ 'settings', {
    playDuration: playDuration,
    workDuration: workDuration,
    playBreakSound: playSound,
    playWorkSound: workSound,
    breakAlert: breakAlert
  },
    {
      headers: {
      "Authorization": window.localStorage["jwt"]
      }
    }
  )
  .then(function (response) {
    console.log(response.data);
    dispath({
      type: C.CHANGE_SETTINGS,
      playDuration: playDuration,
      workDuration: workDuration,
      playPlaySound: playSound,
      playWorkSound: workSound,
      breakAlert: breakAlert
    });
  })
  .catch(function (error) {
    handleError(error);
    console.log(error);
  });
}

export const fetchSettings = () => (dispath) => {
  return axios.get(baseUrl+ 'settings',
    {
      headers: {
      "Authorization": window.localStorage["jwt"]
      }
    }
  )
  .then(function (response) {
    // handle success
    dispath({
      type: C.CHANGE_SETTINGS,
      playDuration: response.data.playDuration,
      workDuration: response.data.workDuration,
      playPlaySound: response.data.playBreakSound,
      playWorkSound: response.data.playWorkSound,
      breakAlert: response.data.breakAlert
    });
  })
  .catch(function (error) {
    // handle error
    console.log(error);
    handleError(error);
  })
  .then(function () {
    // always executed
  });
}

export const removeSession = (id,  date, time, iteration) => (dispath) => {
  document.getElementById('root').classList.toggle("loading-mask");
  axios.delete(baseUrl+ 'sessions/'+ id,
    {
      headers: {
      "Authorization": window.localStorage["jwt"]
      }
    }
  )
  .then(function (response) {
    console.log(response.data);
    document.getElementById('root').classList.toggle("loading-mask");
    dispath(removeStateSession(id));
    dispath(removeDayData(response.data.pomodoroDayData, response.data.bDayRemoved));
  })
  .catch(function (error) {
    console.log(error);
    document.getElementById('root').classList.toggle("loading-mask");
    handleError(error);
  });
};

export const removeStateSession = (id) => ({
  type: C.REMOVE_SESSION,
  id: id
});

export const removeDayData = (pomodoroDayData, bDayRemoved) => ({
  type: C.REMOVE_DAY_DATA,
  pomodoroDayData: pomodoroDayData,
  bDayRemoved: bDayRemoved
});
