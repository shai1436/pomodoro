import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware } from "redux";
import "bootstrap/dist/css/bootstrap.css";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import {
  Route,
  Switch,
  Redirect,
  BrowserRouter as Router
} from "react-router-dom";
import axios from "axios";
import { baseUrl } from "./baseUrl";
import "./stylesheets/index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
//import C from './ActionTypes';
import appReducer from "./reducers";
import basicState from "./initialState";
import Login from "./components/Login";

const initialState = localStorage["pomodoro-store"]
  ? JSON.parse(localStorage["pomodoro-store"])
  : basicState;

const saveState = () => {
  const state = JSON.stringify(store.getState());
  localStorage["pomodoro-store"] = state;
};

const authCheck = () => {
  if (window.localStorage["jwt"]) {
    return axios
      .get(baseUrl + "settings", {
        headers: {
          Authorization: window.localStorage["jwt"]
        }
      })
      .then(function(response) {
        return true;
      })
      .catch(function(error) {
        console.log("error of tge ", error);
        window.location.href = "/";
      });
  } else {
    return false;
  }
};

const ProtectedRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      authCheck() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const store = createStore(
  appReducer,
  initialState,
  applyMiddleware(thunk, logger)
);
window.store = store;
store.subscribe(saveState);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route path="/" exact component={Login} />
        <ProtectedRoute path="/app" component={App} />
        <Route component={Login} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
