import { combineReducers } from 'redux';
import pomodoroSessions from './pomodoroSessions';
import pomodoroSettings from './pomodoroSettings';
import pomodoroDayData from './pomodoroDayData';
import errors from './errors';

export default combineReducers({
  pomodoroSessions,
  errors,
  pomodoroSettings,
  pomodoroDayData
});
