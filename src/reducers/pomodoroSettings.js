import C from '../ActionTypes'
const settings  = (state = {
  "playWorkSound": true,
	"playPlaySound": true,
  "breakAlert": false,
	"workDuration": 25,
	"playDuration": 5
}, action) => {
  switch (action.type) {
    case C.CHANGE_SETTINGS:
      return {
        playWorkSound : action.playWorkSound,
        playPlaySound : action.playPlaySound,
        playDuration : action.playDuration,
        workDuration : action.workDuration,
        breakAlert: action.breakAlert
      };
    default:
      return state
  }
}
export default settings;
