import C from '../ActionTypes'
const dayData = (state = [], action) => {
  switch (action.type) {
    case C.ADD_DAY_DATA:
      if(action.pomodoroDayData){
        let newState = state.filter(dayData =>
          (dayData._id !== action.pomodoroDayData._id)
        );
        return [
          ...newState,
          action.pomodoroDayData
        ].sort((a, b) => new Date(b.date) - new Date(a.date));
      }

      else{
        return state;
      }

    case C.REMOVE_DAY_DATA:

      if(action.pomodoroDayData){
        let newState = state.filter(dayData =>
          (dayData._id !== action.pomodoroDayData._id)
        );

        if(action.bDayRemoved){
          return newState;
        }
        else{
          return [
            ...newState,
            action.pomodoroDayData
          ];
        }
      }
      else{
        return state;
      }

    case C.ADD_ALL_DAY_DATA:
      return action.dayData;

    case C.REMOVE_ALL_DAY_DATA:
      return [];

    default:
      return state;
  }
}

export default dayData;
