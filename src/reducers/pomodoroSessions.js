import C from '../ActionTypes'
const sessions = (state = [], action) => {
  switch (action.type) {
    case C.ADD_SESSION:

      if(action.pomodoroSessions){
        let newState = state.filter(session =>
          (session._id !== action.pomodoroSessions._id)
        );
        return [
          ...newState,
          action.pomodoroSessions
        ].sort((a, b) => new Date(b.date) - new Date(a.date));
      }

      else{
        return state;
      }

    case C.REMOVE_SESSION:
      return state.filter(session =>
        (session._id !== action.id)
      );

    case C.ADD_ALL_SESSIONS:
      return action.sessions;

    case C.REMOVE_ALL_SESSIOS:
      return [];

    default:
      return state;
  }
}

export default sessions;
