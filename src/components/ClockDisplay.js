import CircularProgressbar from "react-circular-progressbar";
import React from "react";
//import Timr from 'timrjs';
import "react-circular-progressbar/dist/styles.css";

const ClockDisplay = ({ percent, text }) => {
  //text = Timr.formatTime(text).formattedTime;
  return (
    <div style={{ width: "250px", margin: "auto" }} className="my-3">
      <CircularProgressbar
        percentage={percent}
        text={text}
        strokeWidth={2}
        styles={{
          path: {
            // Tweak path color:
            stroke: "#ee5253",
            // Tweak path to use flat or rounded ends:
            strokeLinecap: "butt",
            // Tweak transition animation:
            transition: "stroke-dashoffset 0.5s ease 0s"
          }
        }}
      />
    </div>
  );
};

export default ClockDisplay;
