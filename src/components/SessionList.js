import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Input,
  Label,
  Table,
  Card,
  CardBody,
  CardHeader
} from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { FaPlus } from "react-icons/fa";
import Session from "./Session";

const getDate = () => {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = "0" + dd;
  }

  if (mm < 10) {
    mm = "0" + mm;
  }

  today = yyyy + "-" + mm + "-" + dd;
  return today;
};

class SessionList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.handleValidSubmit = this.handleValidSubmit.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  handleTimeChange(event) {
    document.getElementById("work-time").innerHTML =
      event.target.value + " minutes";
    //console.log(event.target.value);
  }

  handleValidSubmit(event) {
    this.toggleModal();
    this.props.addSession(
      this.label.value,
      this.date.value,
      this.iteration.value,
      this.time.value
    );
  }
  handleInvalidSubmit(event) {
    alert("Please fill in the appropriate value");
  }
  render() {
    return (
      <div>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Settings</ModalHeader>
          <ModalBody>
            <AvForm
              onValidSubmit={this.handleValidSubmit}
              onInvalidSubmit={this.handleInvalidSubmit}
            >
              <AvField
                name="label"
                label="Label"
                type="text"
                maxLength="30"
                innerRef={input => (this.label = input)}
                required
                placeholder="Enter a label.."
              />

              <FormGroup>
                <Label htmlFor="time">
                  Time {":  "}
                  <span id="work-time">30 minutes</span>
                </Label>
                <Input
                  type="range"
                  id="time"
                  name="time"
                  min="1"
                  max="90"
                  defaultValue="30"
                  onChange={this.handleTimeChange}
                  innerRef={input => (this.time = input)}
                />
              </FormGroup>

              <AvField
                name="date"
                label="Date"
                type="date"
                placeholder="date placeholder"
                innerRef={input => (this.date = input)}
                value={getDate()}
                max={getDate()}
                required
              />

              <AvField
                name="number"
                label="Iterations"
                type="number"
                min="1"
                max="10"
                innerRef={input => (this.iteration = input)}
                required
              />

              <Button value="submit" color="success">
                Add Session
              </Button>
              <Button
                className="float-right"
                value="Cancel"
                outline
                color="danger"
                onClick={this.toggleModal}
              >
                Cancel
              </Button>
            </AvForm>
          </ModalBody>
        </Modal>
        <Card>
          <CardHeader>
            <div className="row justify-content-between">
              <div className="col col-md-2">
                <h3 className="mb-0">Sessions:</h3>{" "}
              </div>

              <div className="col col-md-2">
                <Button color="success" onClick={this.toggleModal}>
                  {" "}
                  <FaPlus /> Add Session{" "}
                </Button>
              </div>
            </div>
          </CardHeader>
          <CardBody className="p-0">
            <Table className="table-striped mb-0">
              <thead className="thead-light">
                <tr>
                  <th>Activity</th>
                  <th>Iterations/ Time</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                {this.props.sessions.map(session => (
                  <Session
                    key={session._id}
                    {...session}
                    remove={() =>
                      this.props.remove(
                        session._id,
                        session.date,
                        session.time,
                        session.iteration
                      )
                    }
                  />
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default SessionList;
