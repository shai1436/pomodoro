import React, { Component } from "react";
import { baseUrl } from "../baseUrl";
const logo = require("../images/btn_google.png");

class Login extends Component {
  componentWillMount() {
    let token = this.props.location.search.substring(7);
    if (token) {
      console.log("token", token);
      window.localStorage.setItem("jwt", "bearer " + token);
      window.location.href = "/app";
    }
  }
  render() {
    return (
      <div className="pt-5 login">
        <div className="pt-5 container">
          <div className="mt-5">
            <div className="row text-center">
              <div className="col-md-6 offset-md-3 col-xs-12">
                <h1 className="display-3 text-white"> Pomodoro </h1>
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title">
                      Login to improve Your Productivity
                    </h4>
                    <a href={baseUrl + "auth/google"}>
                      <img className="loginBtn" src={logo} alt="Login Button" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
