import React from 'react';

const Footer = () => (
  <div>
    <hr />
    <h6> Developed by Shahrukh Haider </h6>
  </div>
);

export default Footer;
