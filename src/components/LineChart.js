import React from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
  Legend,
  Brush,
  AreaChart,
  Area,
  ResponsiveContainer
} from "recharts";
import { Card, CardBody, CardHeader } from "reactstrap";

const Chart = ({ sessions }) => {
  let startIndex = Math.floor((sessions.length * 90) / 100);
  if (sessions.length < 10) {
    startIndex = 0;
  }
  return (
    <Card className="my-5">
      <CardHeader>
        <div className="row justify-content-start">
          <div className="col col-md-2">
            <h3 className="mb-0">Reports:</h3>
          </div>
        </div>
      </CardHeader>

      <CardBody>
        <ResponsiveContainer width="100%" height={400}>
          <LineChart
            data={sessions}
            margin={{ top: 40, right: 40, bottom: 20, left: 20 }}
          >
            <CartesianGrid vertical={false} />
            <XAxis dataKey="date" />
            <YAxis domain={["auto", "auto"]} />
            <Tooltip />
            <Legend />
            <Line
              dataKey="iteration"
              type="monotone"
              stroke="#ee5253"
              strokeWidth={3}
              dot={false}
            />
            <Line
              dataKey="time"
              type="monotone"
              stroke="#3685B5"
              strokeWidth={3}
              dot={false}
            />
            <Brush dataKey="date" startIndex={startIndex}>
              <AreaChart>
                <CartesianGrid />
                <YAxis hide domain={["auto", "auto"]} />
                <Area dataKey="time" dot={false} />
              </AreaChart>
            </Brush>
          </LineChart>
        </ResponsiveContainer>
      </CardBody>
    </Card>
  );
};

export default Chart;
