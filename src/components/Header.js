import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Input,
  Label,
  Container,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import { baseUrl } from "../baseUrl";
import axios from "axios";
import { FaCog, FaSignInAlt } from "react-icons/fa";
import { MdAddAlarm } from "react-icons/md";

const handleLogout = event => {
  console.log("handleLogout called ");
  axios
    .get(baseUrl + "auth/logout")
    .then(result => {
      console.log("result", result);
      localStorage.clear();
    })
    .catch(error => {
      console.log("Login error", error);
    })
    .then(() => {
      window.location.href = "/";
    });
};

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      dropdownOpen: false
    };
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    //console.log("Settings ", this.props.settings);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  toggleDropdown() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.toggleModal();
    this.props.changeSettings(
      this.play.value,
      this.work.value,
      this.playSound.checked,
      this.workSound.checked,
      this.breakAlert.checked
    );
  }

  handleWorkChange(event) {
    document.getElementById("work-time").innerHTML =
      event.target.value + " minutes";
    //console.log(event.target.value);
  }

  handlePlayChange(event) {
    document.getElementById("play-time").innerHTML =
      event.target.value + " minutes";
    //console.log(event.target.value);
  }

  render() {
    return (
      <div>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>Settings</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.handleSubmit}>
              <FormGroup>
                <Label htmlFor="work">
                  Work Duration {":  "}
                  <span id="work-time">
                    {this.props.settings.workDuration} minutes
                  </span>
                </Label>
                <Input
                  type="range"
                  id="work"
                  name="work"
                  min="1"
                  max="90"
                  defaultValue={this.props.settings.workDuration}
                  onChange={this.handleWorkChange}
                  innerRef={input => (this.work = input)}
                />
              </FormGroup>
              <FormGroup check>
                <Input
                  type="checkbox"
                  id="workSound"
                  name="workSound"
                  defaultChecked={this.props.settings.playWorkSound}
                  innerRef={input => (this.workSound = input)}
                />
                <Label check htmlFor="workSound">
                  Play Session Start Sound?
                </Label>
              </FormGroup>
              <hr />
              <FormGroup>
                <Label htmlFor="play">
                  Break Duration {":  "}
                  <span id="play-time">
                    {this.props.settings.playDuration} minutes
                  </span>
                </Label>
                <Input
                  type="range"
                  min="1"
                  max="20"
                  name="play"
                  id="play"
                  defaultValue={this.props.settings.playDuration}
                  onChange={this.handlePlayChange}
                  innerRef={input => (this.play = input)}
                />
              </FormGroup>
              <FormGroup check>
                <Input
                  type="checkbox"
                  id="playSound"
                  name="playSound"
                  defaultChecked={this.props.settings.playPlaySound}
                  innerRef={input => (this.playSound = input)}
                />
                <Label check htmlFor="playSound">
                  Play Break Start Sound?
                </Label>
              </FormGroup>
              <FormGroup check>
                <Input
                  type="checkbox"
                  id="breakAlert"
                  name="breakAlert"
                  defaultChecked={this.props.settings.breakAlert}
                  innerRef={input => (this.breakAlert = input)}
                />
                <Label check htmlFor="breakAlert">
                  Popup Alert at Session end?
                </Label>
              </FormGroup>
              <hr />
              <Button type="submit" value="submit" color="success">
                Change Settings
              </Button>{" "}
              <Button
                className="float-right"
                value="Cancel"
                outline
                color="danger"
                onClick={this.toggleModal}
              >
                Cancel
              </Button>
            </Form>
          </ModalBody>
        </Modal>
        <nav className="navbar navbar-light bg-light fixed-top p-0">
          <Container>
            <div className="navbar-brand">
              <span className="nav-heading">
                {" "}
                <span className="nav-icon">
                  <MdAddAlarm size={50} color={"#17a2b8"}>
                    {" "}
                  </MdAddAlarm>
                </span>
                POMODORO
              </span>
            </div>
            <div className="pull-right">
              <Dropdown
                isOpen={this.state.dropdownOpen}
                toggle={this.toggleDropdown}
              >
                <DropdownToggle color="info">
                  <span className="navbar-toggler-icon" />
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem>
                    <span onClick={this.toggleModal}>
                      {" "}
                      <FaCog /> Settings
                    </span>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    <span color="info" onClick={handleLogout}>
                      {" "}
                      <FaSignInAlt /> Logout{" "}
                    </span>
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            </div>
          </Container>
        </nav>
      </div>
    );
  }
}

export default Header;
