import React, { Component } from "react";
import { Button, Input } from "reactstrap";
import Timr from "timrjs";
import { FaPlay, FaStop, FaPause } from "react-icons/fa";
import ClockDisplay from "./ClockDisplay";
import { Card, CardBody } from "reactstrap";

let timer;

const StartOrPause = ({ timer }) => {
  let el;
  if (timer.isRunning()) {
    el = (
      <Button color="secondary" onClick={() => timer.pause()}>
        {" "}
        <FaPause /> PAUSE{" "}
      </Button>
    );
  } else {
    el = (
      <Button color="success" onClick={() => timer.start()}>
        {" "}
        <FaPlay /> START{" "}
      </Button>
    );
  }
  return el;
};

const getDate = () => {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = "0" + dd;
  }

  if (mm < 10) {
    mm = "0" + mm;
  }

  today = yyyy + "-" + mm + "-" + dd;
  return today;
};

class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timeVal: this.props.work,
      timeRef: this.props.play,
      timePer: 0,
      labelVal: ""
    };
    //timeRef denotes the next time period
    let time = parseInt(this.props.work, 10);
    timer = Timr(time);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ labelVal: event.target.value });
  }

  componentDidMount() {
    timer.finish(self => {
      if (this.state.timeRef === this.props.play) {
        console.log("inside play");

        if (this.props.settings.playPlaySound)
          document.getElementById("breakSound").play();

        this.setState({
          timeVal: this.props.play,
          timeRef: this.props.work,
          timePer: 0
        });

        self.setStartTime(this.props.play);
        self.start();
      } else {
        if (this.props.settings.breakAlert) {
          alert("The session has ended!");
        }
        console.log("inside work");
        this.setState({ timeVal: this.props.work, timeRef: this.props.play });
        console.log("timeRef ", this.state.timeRef, " work ", this.props.work);
        self.setStartTime(this.props.work);
      }
    });

    timer.ticker(({ formattedTime, percentDone }) => {
      this.setState({ timeVal: formattedTime, timePer: percentDone });
      //console.log("timer: ", formattedTime);
    });

    timer.onStop(self => {
      let minutes = 0;
      if (this.state.timeRef === this.props.work) {
        minutes = parseInt(this.props.work, 10) + parseInt(this.props.play, 10);
      } else {
        minutes =
          parseInt(this.props.work, 10) -
          Math.floor(Timr.timeToSeconds(this.state.timeVal));
        //divide by 60 inside floor;
      }

      console.log(
        "Countdown Stopped seconds",
        Timr.timeToSeconds(this.state.timeVal)
      );

      if (Timr.timeToSeconds(this.state.timeVal) !== 0) {
        if (window.confirm("Add an entry?")) {
          this.props.addSession(
            this.state.labelVal || "No Label",
            getDate(),
            1,
            minutes
          );
        } else {
          // do nothing
        }
        self.setStartTime(this.props.work);
        this.setState({
          timeVal: this.props.work,
          timeRef: this.props.play,
          timePer: 0
        });
      } else if (this.state.timeRef === this.props.work) {
        this.props.addSession(
          this.state.labelVal || "No Label",
          getDate(),
          1,
          minutes
        );
      }
    });
    timer.onPause(self => {
      console.log("Countdown Paused");
      this.forceUpdate();
    });
    timer.onStart(self => {
      if (
        this.props.settings.playWorkSound &&
        this.state.timeRef === this.props.play
      )
        document.getElementById("startSound").play();
    });
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.work !== prevProps.work ||
      this.props.play !== prevProps.play
    ) {
      this.setState({
        timeVal: this.props.work,
        timeRef: this.props.play,
        timePer: 0,
        labelVal: ""
      });
      timer.setStartTime(this.props.work);
    }
  }
  render() {
    return (
      <Card className="my-5 py-5 bg-light">
        <CardBody>
          <div>
            <div className="row justify-content-center">
              <div className="col col-md-6 col-sm-10 ">
                <Input
                  type="text"
                  name="label"
                  id="label"
                  className="label-input"
                  onChange={this.handleChange}
                  placeholder="Enter a label.."
                />
              </div>
            </div>
            <ClockDisplay
              percent={this.state.timePer}
              text={this.state.timeVal}
            />
            <StartOrPause timer={timer} />{" "}
            <Button
              color="danger"
              disabled={!timer.isRunning()}
              outline
              onClick={() => timer.stop()}
            >
              {" "}
              <FaStop /> STOP{" "}
            </Button>
          </div>
        </CardBody>
      </Card>
    );
  }
}

export default Clock;
