import React from "react";
import { Button } from "reactstrap";
import { FaTrash } from "react-icons/fa";

const Session = ({ label, date, iteration, time, remove }) => (
  <tr>
    <td className="align-middle">
      <span>{label} </span>
    </td>
    <td className="align-middle">
      {" "}
      <span>
        {iteration} Sessions/ {time} Minutes{" "}
      </span>
    </td>
    <td>
      <Button color="danger" outline onClick={remove}>
        {" "}
        <FaTrash />{" "}
      </Button>
    </td>
  </tr>
);

export default Session;
